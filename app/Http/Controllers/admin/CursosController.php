<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Http\Response;

//Model
use App\Models\Cursos;

class CursosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //SELECIONAR TODOS OS CURSOS PARA SEREM LISTADOS
        $cursos = Cursos::all();
        //VIEWS PARA MONTAR O INDEX DE CURSOS
        return view('admin.cursos.index_cursos', compact('cursos'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //MOSTRAR A VIEW PARA O FORMULÁRIO DE CURSO
        return view('admin.cursos.create_curso');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //SALVAR OS DADOS DO CURSO NO BANCO DE DADOS
        $curso = array();
        $curso['nome'] = $request['nome_curso'];
        $curso['valor'] = $request['preco_curso'];

        //INSERINDO NO BANCO DE DADOS
        $create = Cursos::create($curso);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //MOSTRAR UM CURSO ESPECÍFICO

        //PEGAR OS DADOS DO CURSO ESPECÍFICO
        $curso = Cursos::where('id_curso', $id)->get();
        return view('', compact('curso'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //ABRIR A TELA OU POPUP PARA EDITAR JÁ COM AS INFORMAÇÕES
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //FAZER A ATUALIZAÇÃO PARA O BANCO DE DADOS
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //REMOVER DO BANDO DE DADOS
        $curso = Cursos::where('id_curso', $id);
        $curso->delete();
    }

}
