<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Cursos extends Model
{
    use HasFactory;

    protected $tables = 'cursos';

    //O QUE PODE SER INSERIDO NA TABELA 'cursos', BASEDO NESSA MODEL 'Cursos'
    protected $fillable = [
        'nome',
        'valor',
    ];
}
