<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Admin\CursosController;

//Controllers ==========================================================================================================

//======================================================================================================================

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::prefix('admin')->group(function(){

    Route::prefix('cursos')->group(function(){

        Route::get( "/",           [CursosController::class, 'index']);
        Route::get("/create",      [CursosController::class, 'create']);
        Route::post("/",           [CursosController::class, 'store']);
        Route::get( "/{id}",       [CursosController::class, 'show']);

        Route::post( "/{id}/delete",    [CursosController::class, 'destroy']);

    });

});
