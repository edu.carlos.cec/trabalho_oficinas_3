<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAgendamentosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('agendamentos', function (Blueprint $table) {
            //ID AGENDAMENTO
            $table->id('id_agendamento');

            //FK DE USUÁRIO
            $table->unsignedBigInteger('id_usuario');
            $table->foreign('id_usuario')->references('usuarios')->on('id_usuario');

            //FK DE SERVIÇO
            $table->unsignedBigInteger('id_servico');
            $table->foreign('id_servico')->references('servicos')->on('id_servico');

            //STATUS
            $table->boolean('status');

            //CRIAÇÃO OU ATUALIZAÇÃO
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('agendamentos');
    }
}
