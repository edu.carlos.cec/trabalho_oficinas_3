<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsuariosCursosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('usuarios_cursos', function (Blueprint $table) {

            //FK DE USUARIO
            $table->unsignedBigInteger('id_usuario');
            $table->foreign('id_usuario')->references('usuarios')->on('id_usuario');

            //FK DE CURSO
            $table->unsignedBigInteger('id_curso');
            $table->foreign('id_curso')->references('cursos')->on('id_curso');

            //DATA DE CRIACAO DA INSCRICAO
            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('usuarios_cursos');
    }
}
