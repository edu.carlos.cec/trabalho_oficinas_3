<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('usuarios', function (Blueprint $table) {

            //ID USUARIO
            $table->id('id_usuario');

            //NOME DO USUARIO PARA LOGIN
            $table->string('name')->unique();

            //SENHA DO USUARIO
            $table->string('password');

            //NOME DO USUARIO
            $table->string('nome_usuario');

            //IDADE DO USUARIO
            $table->integer('idade');

            //ENDERECO
            $table->string('endereco');

            //EMAIL DO USUARIO
            $table->string('email')->unique();

            $table->timestamp('email_verified_at')->nullable();

            $table->rememberToken();

            //CRIACAO E ATUALIZACAO DO USUARIO
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('usuarios');
    }
}
