<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateServicosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('servicos', function (Blueprint $table) {
            //ID SERVICO
            $table->id('id_servicos');

            //DESCRICAO DO SERVIÇO
            $table->string('descricao');

            //VALOR DO SERVIÇO
            $table->float('valor',  8, 2);

            //CRIAR OU ATUALIZAÇÃO
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('servicos');
    }
}
