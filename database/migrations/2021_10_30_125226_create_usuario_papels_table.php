<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsuarioPapelsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('usuario_papel', function (Blueprint $table) {

            //FK DE USUARIO
            $table->unsignedBigInteger('id_usuario');
            $table->foreign('id_usuario')->references('usuarios')->on('id_usuario');

            //FK PAPEL
            $table->unsignedBigInteger('id_papel');
            $table->foreign('id_papel')->references('papeis')->on('id_papel');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('usuario_papels');
    }
}
