<!DOCTYPE html>

<html>
    <head></head>

    <body>
        <h1>Cursos</h1>
        <a href="/admin/cursos/create">Criar novo curso</a>
        <table>
            <thead>
                <tr>
                    <th>Nome Curso</th>
                    <th>Valor</th>
                    <th>Criado em </th>
                    <th>Atualizado em </th>
                    <th>Editar</th>
                    <th>Remover</th>
                </tr>
            </thead>

            <tbody>
                @foreach($cursos as $curso)
                    <tr>
                        <td>{{$curso->nome}}</td>
                        <td>{{$curso->valor}}</td>
                        <td>{{$curso->created_at}}</td>
                        <td>{{$curso->updated_at}}</td>
                        <td>{{$curso->updated_at}}</td>
                        <td><a href={{url("/admin/cursos/$curso->id_curso")}}>Editar</a></td>

                        <td>
                            <form action="{{url("/admin/cursos/$curso->id_curso/delete")}}" method="POST">
                                {{csrf_field()}}
                                <button type="submit">Destruir</button>
                            </form>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </body>
</html>
